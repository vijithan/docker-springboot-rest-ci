package com.restbackend.rest.backend

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RestBackendApplication {

	static void main(String[] args) {
		SpringApplication.run RestBackendApplication, args
	}
}
